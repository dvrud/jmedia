console.log("running");
Stripe.setPublishableKey("pk_test_CRhKfBjNYTKQbSXmxuMoixPI");

const $form = $("#checkout-form");

$form.submit(function(event) {
  $(".charge-error").addClass("hidden");
  $form.find("#checkout-button").prop("disabled, true");
  Stripe.card.createToken(
    {
      number: $("#creditno").val(),
      cvc: $("#cvv").val(),
      exp_month: $("#expm").val(),
      exp_year: $("#expy").val(),
      name: $("#card-name").val()
    },
    stripeResponseHandler
  );
  return false;
});

function stripeResponseHandler(status, response) {
  if (response.error) {
    // Problem!

    // Show the errors on the form
    $(".charge-error").text(response.error.message);
    $(".charge-error").removeClass("hidden");

    $form.find("button").prop("disabled", false); // Re-enable submission
  } else {
    // Token was created!

    // Get the token ID:
    var token = response.id;

    // Insert the token into the form so it gets submitted to the server:
    $form.append($('<input type="hidden" name="stripeToken" />').val(token));

    // Submit the form:
    $form.get(0).submit();
  }
}
