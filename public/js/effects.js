window.addEventListener(
  "scroll",
  function() {
    let target = document.querySelector("#main-navbar");
    if (window.pageYOffset > 600) {
      target.style.transition = "1.5s";
      target.classList.add("bg-dark-custom");
      // target.style.transition = '2s';
      // target.style.display = 'block';
      // target.style.transition = '1s';
    } else {
      target.classList.remove("bg-dark-custom");
      target.style.transition = "1.5s";

      // target.style.display = 'none';
      // target.style.transition = '1s';
    }
  },
  false
);

// let section = document.querySelector('#scroll-event'),
//     sectionOffSet = section.offsetTop,
//     finalOff = sectionOffSet;

//     console.log(sectionOffSet);

//     window.addEventListener('scroll' , effects);

//     function effects() {
//       if(window.pageYOffset > sectionOffSet-300){
//         section.style.opacity = "0.5";
//       }
//     };
