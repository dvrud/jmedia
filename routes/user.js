const express = require("express");
const mongoose = require("mongoose");
// bcrypt to hash the password
const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const passport = require("passport");
const async = require("async");
const nodemailer = require("nodemailer");
const crypto = require("crypto");
const router = express.Router();
// Bringed it in to protect the routes.
const { ensureAuthenticated } = require("../helpers/auth");
// Body parser middleware -- descr -- It is used to get the data from the form to process.
// in short we can access the data from thr form through req object.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

// Loading blog model
require("../models/Blogs");
const Blogs = mongoose.model("blogs");
// Loading productdata model
require("../models/products");
const productData = mongoose.model("products");
// Loading userdata  model
require("../models/users");
const Users = mongoose.model("users");
// Loading orders model
const Orders = require("../models/orders");
// Loading cart modle
const Cart = require("../models/cart");
// Registration route
router.get("/user/signup", (req, res, next) => {
  res.render("users/signup");
});

// UserRegistration form processing
router.post("/user/signup", (req, res, next) => {
  // Checking for the validations
  let errors = [];
  const specialCode = "specialAdmin100";
  let booleanAdmin;
  if (req.body.password != req.body.cpassword) {
    errors.push({ text: "Passwords do not match !" });
  }

  if (req.body.password.length < 7) {
    errors.push({ text: "Password should be atleast 7 characters !" });
  }

  if (req.body.checkedadmincode) {
    if (req.body.specialcode !== specialCode) {
      errors.push({ text: "Admin Code does not match , please try again" });
    } else {
      booleanAdmin = true;
    }
  }

  if (errors.length > 0) {
    // if errors , sending user back to the page with all the fields filled with the previous data.
    res.render("users/signup", {
      errors: errors,
      email: req.body.email,
      password: req.body.password,
      cpassword: req.body.cpassword
    });
  }
  // or else letting the register method work
  else {
    // Checking if the email already exist.
    Users.findOne({ email: req.body.email }).then(user => {
      if (user) {
        console.log(`Email is already registered with us ${user}`);
        req.flash("error_msg", "Email is already registere with us");
        res.redirect("/user/login");
      }
      // If email does not exist letting the registration process continue.
      else {
        const newUser = new Users({
          email: req.body.email,
          password: req.body.password,
          name: req.body.name,
          address: req.body.address,
          mobile: req.body.mobile,
          isAdmin: booleanAdmin
        });
        // Hashing the password through bcrypt js.
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => {
                req.flash("success_msg", "You are registered and can login");
                res.redirect("/user/login");
              })
              .catch(err => {
                console.log(err);
                return;
              });
          });
        });
      }
    });
  }
});

// User login route.
router.get("/user/login", (req, res) => {
  res.render("users/signin");
});

// user login form processing
router.post(
  "/user/login",
  (req, res, next) => {
    passport.authenticate("local", {
      failureRedirect: "/user/login",
      failureFlash: true
    })(req, res, next);
    // req res and next are put in side because we gotta immediately fire off the passport authenticate function.
  },
  (req, res) => {
    if (req.session.oldUrl) {
      res.redirect(req.session.oldUrl);
      req.session.oldUrl = null;
    } else {
      if (req.user.isAdmin) {
        res.redirect("/admin");
      } else {
        res.redirect("/user/userprofile");
      }
    }
  }
);

// logout route
router.get("/user/logout", (req, res) => {
  req.session.destroy();

  req.logOut();
  // req.flash("success_msg", "Successfully logged out.");

  // req.session.destroy(function() {
  //   res.clearCookie("connect.sid");
  //   res.redirect("/user/login");
  // });
  res.redirect("/user/login");
});

//User profile route
router.get("/user/userprofile", ensureAuthenticated, (req, res) => {
  // res.header(
  //   "Cache-Control",
  //   "no-cache, private, no-store, must-revalidate,max-stale=0, post-check=0, pre-check=0"
  // );
  // Orders.find({ user: req.user }).then(orders => {
  //   let cart;
  //   orders.forEach(order => {
  //     cart = new Cart(order.cart);
  //     order.items = cart.generateArray();
  //   });
  //   res.render("users/userprofile", { orders: orders });
  // });
  res.render("users/userprofile");
});

// Getting user specific orders
router.get("/user/order", (req, res) => {
  res.header(
    "Cache-Control",
    "no-cache, private, no-store, must-revalidate,max-stale=0, post-check=0, pre-check=0"
  );
  Orders.find({ user: req.user }).then(orders => {
    let cart;
    orders.forEach(order => {
      cart = new Cart(order.cart);
      order.items = cart.generateArray();
    });
    res.render("users/userorders", { orders: orders });
  });
});
// Get the users specific blogs
router.get("/userblogs", (req, res) => {
  const loggeduser = req.user.id;
  Blogs.find({
    user: loggeduser
  }).then(blogs => {
    res.render("users/userblogs", {
      blogs: blogs
    });
  });
});

// Forgot password route
router.get("/forgotpwd", (req, res) => {
  res.render("users/forgotpassword");
});

// Forgot password form processing

router.post("/forgotpwd", (req, res, next) => {
  async.waterfall(
    [
      done => {
        crypto.randomBytes(20, (err, buf) => {
          const token = buf.toString("hex");
          done(err, token);
        });
      },
      (token, done) => {
        Users.findOne({ email: req.body.email }, (err, user) => {
          if (!user) {
            req.flash(
              "error_msg",
              "No account with that email is registered with us , please try with valid email address."
            );
            return res.redirect("/forgotpwd");
          }

          user.resetPasswordToken = token;
          user.resetPasswordExpires = Date.now() + 3600000;

          user.save(err => {
            done(err, token, user);
          });
        });
      },
      (token, user, done) => {
        const smtpTransport = nodemailer.createTransport({
          service: "Outlook",
          auth: {
            user: "jmediaproduction@outlook.com",
            pass: "Jmedia@123"
          }
        });

        const mailOptions = {
          to: user.email,
          from: "jmediaproduction@outlook.com",
          subject: "Password Reset Link.",
          text: `You are receiving this because you (or someone else) have requested the reset of password.Please click on the below link to reset the password. http://localhost:5000/reset/${token}`
        };

        smtpTransport.sendMail(mailOptions, err => {
          console.log("Mail Sent");
          req.flash(
            "success_msg",
            `An email has been sent to ${user.email} with further instructions.`
          );
          done(err, "done");
        });
      }
    ],
    err => {
      if (err) return next(err);
      res.redirect("/forgotpwd");
    }
  );
});
// Forgot password reset token route
router.get("/reset/:token", (req, res, next) => {
  Users.findOne(
    {
      resetPasswordToken: req.params.token,
      resetPasswordExpires: { $gt: Date.now() }
    },
    (err, user) => {
      if (!user) {
        req.flash("error", "Password reset token is invalid or has expired.");
        return res.redirect("/forgotpwd");
      }
      res.render("users/reset", { token: req.params.token });
    }
  );
});

// Reset password form processing
router.post("/reset/:token", (req, res) => {
  Users.findOne({
    resetPasswordToken: req.params.token,
    resetPasswordExpires: { $gt: Date.now() }
  }).then(user => {
    if (!user) {
      req.flash(
        "error_msg",
        "Invalid Password reset Token or link has expired"
      );
      res.redirect("/forgotpwd");
    }

    if (req.body.pass === req.body.cpass) {
      // Hashing the password through bcrypt js.
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.pass, salt, (err, hash) => {
          if (err) throw err;
          user.password = hash;
          user
            .save()
            .then(user => {
              req.flash("success_msg", "Your password has been changed");
              res.redirect("/user/login");
            })
            .catch(err => {
              console.log(err);
              return;
            });
        });
      });
    }
  });
});
// // Reset password post req
// router.post("/reset/:token", function(req, res) {
//   async.waterfall(
//     [
//       function(done) {
//         Users.findOne(
//           {
//             resetPasswordToken: req.params.token,
//             resetPasswordExpires: { $gt: Date.now() }
//           },
//       //     function(err, user) {
//       //       if (!user) {
//       //         req.flash(
//       //           "error",
//       //           "Password reset token is invalid or has expired."
//       //         );
//       //         return res.redirect("back");
//       //       }
//       //       if (req.body.pass === req.body.cpass) {
//       //         user.setPassword(req.body.pass, function(err) {
//       //           user.resetPasswordToken = undefined;
//       //           user.resetPasswordExpires = undefined;

//       //           user.save(function(err) {
//       //             req.logIn(user, function(err) {
//       //               done(err, user);
//       //             });
//       //           });
//       //         });
//       //       } else {
//       //         req.flash("error", "Passwords do not match.");
//       //         return res.redirect("back");
//       //       }
//       //     }
//       //   );
//       // },

//       function(user, done) {
//         var smtpTransport = nodemailer.createTransport({
//           service: "Outlook",
//           auth: {
//             user: "jmediaproduction@outlook.com",
//             pass: "Jmedia@123"
//           }
//         });
//         var mailOptions = {
//           to: user.email,
//           from: "jmediaproduction@outlook.com",
//           subject: "Your password has been changed",
//           text:
//             "Hello,\n\n" +
//             "This is a confirmation that the password for your account " +
//             user.email +
//             " has just been changed.\n"
//         };
//         smtpTransport.sendMail(mailOptions, function(err) {
//           req.flash("success", "Success! Your password has been changed.");
//           done(err);
//         });
//       }
//     ],
//     function(err) {
//       res.redirect("/campgrounds");
//     }
//   );
// });

module.exports = router;
