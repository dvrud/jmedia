const express = require("express");
const mongoose = require("mongoose");
// bcrypt to hash the password
const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const passport = require("passport");
const router = express.Router();
// Bringed it in to protect the routes.
const { ensureAuthenticated } = require("../helpers/auth");
// Body parser middleware -- descr -- It is used to get the data from the form to process.
// in short we can access the data from thr form through req object.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.get("/", (req, res) => {
  res.header(
    "Cache-Control",
    "no-cache, private, no-store, must-revalidate,max-stale=0, post-check=0, pre-check=0"
  );
  res.render("index");
});

router.get("/usermessage", (req, res) => {
  // let formerror = [];
  // if (!req.body.msgname) {
  //   formerror.push({ text: "Please enter the name" });
  // }
  // if (!req.body.msgemail) {
  //   formerror.push({ text: "Please enter the email" });
  // }
  // if (!req.body.msgmsg) {
  //   formerror.push({ text: "Please enter the message" });
  // }
  // if (formerror.length > 0) {
  //   res.render("index", {
  //     formerror: formerror
  //   });
  //   formerror = [];
  // } else {
  res.render("users/usermessage");
  // }
});
module.exports = router;
