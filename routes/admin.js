const express = require("express");
const mongoose = require("mongoose");
// bcrypt to hash the password
const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require("path");
const multer = require("multer");
const router = express.Router();
// Bringed it in to protect the routes.
const { ensureAuthenticated } = require("../helpers/auth");

const { ensureAdminAuthenticated } = require("../helpers/auth");
// Body parser middleware -- descr -- It is used to get the data from the form to process.
// in short we can access the data from thr form through req object.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(express.static(path.join(__dirname, "/public")));

// Multer middleware
const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  dest: "uploads/",
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});
// Getting in the databse
// products model
const Products = require("../models/products");
// Blog models
require("../models/Blogs");
const Blogs = mongoose.model("blogs");
// Blog approval schema
require("../models/adminblogapproval");
const Blogapproval = mongoose.model("adminblogapproval");
// Bringing in the admin users model
const Admin = require("../models/admins");
// Bringing in the imagepath albums model
require("../models/imagepath");
const Album = mongoose.model("imagepath");
// Admin route
router.get("/admin", (req, res) => {
  Blogapproval.count({}).then(count => {
    res.render("admin/admin", {
      count: count
    });
  });
});

// For adding the products
// router.get("/admin/add", ensureAdminAuthenticated, (req, res, next) => {
//   res.render("admin/productadd");
// });

// For adding the blogs
router.get("/user/addblogs", ensureAuthenticated, (req, res) => {
  res.render("admin/Blogadd");
});

// Blog approval route
router.get("/adminbloglist", (req, res) => {
  Blogapproval.find({}).then(blogs => {
    res.render("admin/blogapproval", {
      blogs: blogs
    });
  });
});
// Blog approval process route
router.get("/approveblog/:id", (req, res) => {
  Blogapproval.findOne({
    _id: req.params.id
  }).then(blog => {
    console.log(blog.title);
    const approvedBlog = {
      title: blog.title,
      content: blog.content,
      image: blog.image,
      author: blog.author,
      user: blog.user
    };

    new Blogs(approvedBlog)
      .save()
      .then(blog => {
        Blogapproval.deleteOne({
          _id: req.params.id
        }).then(done => {
          console.log(done);
        });
        res.redirect("/blogs");
      })
      .catch(err => {
        console.log(err);
      });
  });
});

// User Album display
router.get("/adminviewalbum", (req, res) => {
  Album.find({}).then(users => {
    res.render("admin/viewalbum", {
      users: users
    });
  });
});
// Process the productadd form.
// router.post("/productadd", (req, res) => {
//   const newProduct = {
//     imagePath: "https://source.unsplash.com/random/480*480",
//     title: req.body.title,
//     description: req.body.description,
//     price: req.body.price
//   };

//   new Products(newProduct).save().then(product => {
//     res.redirect("/admin");
//   });
// });

// Process the blog form.
// router.post("/addblogs", upload.single("image"), (req, res) => {
//   let errors = [];

//   if (!req.body.title) {
//     errors.push({ text: "Please add title" });
//   }
//   if (!req.body.content) {
//     errors.push({ text: "Please add Content" });
//   }
//   if (!req.body.author) {
//     errors.push({ text: "Please add Author" });
//   }

//   if (errors.length > 0) {
//     res.render("admin/Blogadd", {
//       errors: errors,
//       title: req.body.title,
//       content: req.body.content,
//       author: req.body.author,
//       image: req.file.path
//     });
//   } else {
//     const newBlog = {
//       title: req.body.title,
//       content: req.body.content,
//       author: req.body.author,
//       image: req.file.path
//     };
//     new Blogs(newBlog).save().then(blog => {
//       res.redirect("/admin");
//     });
//   }
// });

// Displaying product list to admin to edit and delete the products.
// router.get("/admin/productlist", (req, res) => {
//   Products.find({})
//     .sort({ time: "desc" })
//     .then(products => {
//       res.render("admin/productlist", {
//         products: products
//       });
//     });
// });
module.exports = router;
