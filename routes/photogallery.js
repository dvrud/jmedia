const express = require("express");
const mongoose = require("mongoose");
// bcrypt to hash the password
const gallery = require("express-photo-gallery");

const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require("path");
const multer = require("multer");
const router = express.Router();
// Bringed it in to protect the routes.
const { ensureAdminAuthenticated } = require("../helpers/auth");
const { ensureAuthenticated } = require("../helpers/auth");
// Body parser middleware -- descr -- It is used to get the data from the form to process.
// in short we can access the data from thr form through req object.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(express.static(path.join(__dirname, "/public")));

// Multer middleware
const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  dest: "uploads/",
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

// Loading gallery model
require("../models/imagepath");
const imagepath = mongoose.model("imagepath");
// Loading users model
const Users = mongoose.model("users");
// Bringing in the imagepath albums model
require("../models/imagepath");
const Album = mongoose.model("imagepath");
router.get("/admin/photogallery", (req, res) => {
  Users.find({}).then(users => {
    res.render("admin/photogallery", { users: users });
  });
});
router.post(
  "/admin/photogalleryadd",
  upload.array("image", 100),
  (req, res) => {
    // const ids = Users.findById(req.body.reqid)
    //             .then(users => {

    //             })
    // if(req)
    const imagealbum = {
      image: req.files,
      user: req.body.nameid
    };
    console.log(req.body.nameid);
    new imagepath(imagealbum).save().then(blog => {
      req.flash("success_msg", "Image album successfully updated");
      // console.log(req.files);
      res.redirect("/admin");
    });
  }
);

router.get("/userphotogallerydisplay", (req, res) => {
  imagepath
    .find({
      // user: req.user.email
    })
    .then(paths => {
      console.log(paths);
      res.render("users/gallery", {
        paths: paths
      });
    });
});
const options = {
  title: `My awesome photogallery`
};

router.delete("/delete/:id/:filename", (req, res) => {
  console.log(req.params.id, req.params.filename);
  Album.findByIdAndUpdate(
    req.params.id,
    {
      $pull: {
        image: { filename: req.params.filename }
      }
    },
    function(err, model) {
      if (err) {
        console.log("error");
      }
      console.log(model, "done");
    }
  );
});
// router.use("/photos", gallery("./uploads", options));

module.exports = router;
