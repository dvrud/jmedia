const express = require("express");
const mongoose = require("mongoose");
// bcrypt to hash the password
const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const passport = require("passport");
const router = express.Router();
// Bringed it in to protect the routes.
const { ensureAuthenticated } = require("../helpers/auth");
// Body parser middleware -- descr -- It is used to get the data from the form to process.
// in short we can access the data from thr form through req object.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
// Loading blog model
require("../models/Blogs");
const Blogs = mongoose.model("blogs");
router.get("/portfolio", (req, res) => {
  res.render("portfolio");
});

// portfolio details route
router.get("/portfoliodetails", (req, res) => {
  Blogs.find({}).then(blogs => {
    res.render("portfoliodetails", {
      blogs: blogs
    });
  });
});
module.exports = router;
