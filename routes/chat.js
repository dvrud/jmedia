const express = require("express");
const mongoose = require("mongoose");
// bcrypt to hash the password
const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const passport = require("passport");
const router = express.Router();
const server = require("http").createServer(router);
const io = require("socket.io").listen(server);
users = [];
connections = [];
// Bringed it in to protect the routes.
const { ensureAuthenticated } = require("../helpers/auth");
// Body parser middleware -- descr -- It is used to get the data from the form to process.
// in short we can access the data from thr form through req object.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.get("/livechat", (req, res) => {
  res.render("users/chat");
});

io.sockets.on("connection", socket => {
  connections.push(socket);
  console.log("Connected: %s sockets connected", connections.length);

  // Disconnect
  socket.on("disconnect", data => {
    users.splice(users.indexOf(socket.username), 1);
    updateUsernames();
    connections.splice(connections.indexOf(socket), 1);
    console.log("Disconnected: %s sockets disconnected", connections.length);
  });

  // Send messages
  socket.on("send message", data => {
    console.log(data);
    io.sockets.emit("new message", {
      msg: data,
      user: socket.username
    });
  });

  // New User
  socket.on("new user", (data, cb) => {
    cb(true);
    socket.username = data;
    users.push(socket.username);
    updateUsernames();
  });

  const updateUsernames = () => {
    io.sockets.emit("get users", users);
  };
});

server.listen(4000);
module.exports = router;
