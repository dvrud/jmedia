const express = require("express");
const mongoose = require("mongoose");
// bcrypt to hash the password
const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const methodOverride = require("method-override");
const passport = require("passport");
const path = require("path");
const multer = require("multer");
const router = express.Router();
// Bringed it in to protect the routes.
const { ensureAuthenticated } = require("../helpers/auth");
const { ensureAdminAuthenticated } = require("../helpers/auth");
// Body parser middleware -- descr -- It is used to get the data from the form to process.
// in short we can access the data from thr form through req object.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(express.static(path.join(__dirname, "/public")));
router.use(methodOverride("_method"));

// Multer middleware
const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  dest: "uploads/",
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

// Loading blog model
require("../models/Blogs");
const Blogs = mongoose.model("blogs");
// Loading approve blog models
require("../models/adminblogapproval");
const adminBlogApproval = mongoose.model("adminblogapproval");

// blog route
router.get("/blogs", (req, res) => {
  res.header(
    "Cache-Control",
    "no-cache, private, no-store, must-revalidate,max-stale=0, post-check=0, pre-check=0"
  );
  // Getting the blogs from the database and rendering it to the blog page to the display the blogs.and also sorting throught the date
  Blogs.find({})
    .sort({ date: "desc" })
    .then(blogs => {
      res.render("blogs", {
        blogs: blogs
      });
    });
});

// Process the blog form.
router.post("/blogadd", upload.single("image"), (req, res) => {
  let errors = [];

  if (!req.body.title) {
    errors.push({ text: "Please add title" });
  }
  if (!req.body.content) {
    errors.push({ text: "Please add Content" });
  }
  if (!req.body.author) {
    errors.push({ text: "Please add Author" });
  }

  if (errors.length > 0) {
    res.render("admin/Blogadd", {
      errors: errors,
      title: req.body.title,
      content: req.body.content,
      author: req.body.author
    });
  } else {
    const newBlog = {
      title: req.body.title,
      content: req.body.content,
      author: req.body.author,
      image: req.file.path,
      user: req.user.id
    };
    new adminBlogApproval(newBlog).save().then(blog => {
      req.flash(
        "success_msg",
        "Blog is Under review , it will be published as soon as it is reveiewed"
      );
      console.log(req.files);
      res.redirect("/blogs");
    });
  }
});
// Showing full blog
router.get("/blogexpand:id", (req, res) => {
  Blogs.findOne({
    _id: req.params.id
  }).then(blog => {
    res.render("expandedblog", {
      blog: blog
    });
  });
});

// // Edit blog route
// router.get("/blog/edit/:id", (req, res) => {
//   Blogs.findOne({
//     _id: req.params.id
//   }).then(blog => {
//     res.render("users/editblog", {
//       blog: blog
//     });
//   });
// });

// // Edit Blog form processing
// router.put("/blog/edit/:id", upload.single("image"), (req, res) => {
//   Blogs.findOne({
//     _id: req.params.id
//   }).then(blog => {
//     // Storing new values
//     blog.title = req.body.title;
//     blog.content = req.body.content;
//     blog.image = req.file.path;
//     blog.author = req.body.author;

//     blog.save().then(blog => {
//       res.redirect("/blogs");
//     });
//   });
// });
module.exports = router;
