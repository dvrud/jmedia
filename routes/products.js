const express = require("express");
const mongoose = require("mongoose");
// bcrypt to hash the password
const bcrypt = require("bcryptjs");
const bodyParser = require("body-parser");
const passport = require("passport");
const router = express.Router();
// Bringed it in to protect the routes.
const { ensureAuthenticated } = require("../helpers/auth");
// Body parser middleware -- descr -- It is used to get the data from the form to process.
// in short we can access the data from thr form through req object.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

// Loading productdata model
require("../models/products");
const productData = mongoose.model("products");
// Loading userdata  model
require("../models/users");
const Users = mongoose.model("users");
// Loading cart model

const Cart = require("../models/cart");
// Order model loading
const Order = require("../models/orders");

// Index route
router.get("/products", (req, res, next) => {
  const successMsg = req.flash("success_msg")[0];
  res.header(
    "Cache-Control",
    "no-cache, private, no-store, must-revalidate,max-stale=0, post-check=0, pre-check=0"
  );
  // Getting the products from the database and rendering it to the product page to the display the products.and also sorting through the date
  productData
    .find({})
    .sort({ time: "desc" })
    .then(products => {
      res.render("shop/product", {
        products: products,
        successMsg: successMsg,
        noMessage: !successMsg
      });
    });
});

// Single product view route
// router.get("/:id", (req, res) => {
//   productData
//     .findOne({
//       _id: req.params.id
//     })
//     .then(product => {
//       console.log(product);
//       res.render("shop/viewproduct", {
//         product: product
//       });
//     })
//     .catch(err => {
//       console.log(err);
//     });
// });
// add tocart route
router.get("/addtocart/:id", (req, res) => {
  let productId = req.params.id;
  let cart = new Cart(req.session.cart ? req.session.cart : {});

  productData
    .findById(productId)
    .then(product => {
      cart.add(product, product.id);
      req.session.cart = cart;
      // console.log(req.session.cart);
      res.redirect("/products");
    })
    .catch(err => {
      console.log(err);
    });
});

// Reduce itme route
router.get("/reduce/:id", (req, res) => {
  const productId = req.params.id;
  const cart = new Cart(req.session.cart ? req.session.cart : {});

  cart.reduceByOne(productId);
  req.session.cart = cart;
  res.redirect("/shopping-cart");
});

router.get("/remove/:id", (req, res) => {
  const productId = req.params.id;
  const cart = new Cart(req.session.cart ? req.session.cart : {});

  cart.removeItem(productId);
  req.session.cart = cart;
  res.redirect("/shopping-cart");
});
// Shopping cart page route
router.get("/shopping-cart", (req, res) => {
  if (!req.session.cart) {
    return res.render("shop/cart", { products: null });
  }
  const cart = new Cart(req.session.cart);
  res.render("shop/cart", {
    products: cart.generateArray(),
    totalPrice: cart.totalPrice
  });
});

// checkout page route
router.get("/checkout", ensureAuthenticated, (req, res) => {
  if (!req.session.cart) {
    return res.redirect("/shopping-cart");
  }
  const cart = new Cart(req.session.cart);
  const errMsg = req.flash("error_msg")[0];
  res.render("shop/checkout", {
    total: cart.totalPrice,
    errMsg: errMsg,
    noError: !errMsg
  });
});
// Checkout form processing
router.post("/checkout", ensureAuthenticated, (req, res) => {
  if (!req.session.cart) {
    return res.redirect("/shopping-cart");
  }
  const cart = new Cart(req.session.cart);
  // Set your secret key: remember to change this to your live secret key in production
  // See your keys here: https://dashboard.stripe.com/account/apikeys
  const stripe = require("stripe")("sk_test_k2YWHjA0UuKcWD2ncoGMx6Ev");

  // Token is created using Checkout or Elements!
  // Get the payment token ID submitted by the form:
  const token = req.body.stripeToken; // Using Express

  const charge = stripe.charges.create(
    {
      amount: cart.totalPrice * 1000,
      currency: "INR",
      description: "Example charge",
      source: token
      // ,metadata: { order_id: 6735 }
    },
    (err, charge) => {
      if (err) {
        req.flash("error_msg", err.message);
        return res.redirect("/checkout");
      }

      // Orders model
      const order = new Order({
        user: req.user,
        cart: cart,
        address: req.body.address,
        name: req.body.name,
        paymentId: charge.id
      });
      order
        .save()
        .then(err => {
          req.flash("success_msg", "Succesfully Bought the product.");
          req.session.cart = null;
          res.redirect("/products");
        })
        .catch(err => {
          console.log(err);
        });
    }
  );
});
module.exports = router;
