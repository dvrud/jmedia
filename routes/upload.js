const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const bodyParser = require("body-parser");
const multer = require("multer");
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(express.static(path.join(__dirname, "/public")));
// const upload = multer({ dest: "uploads/" });

// const filename = (req, file, cb) => {
//   cb(null, new Date().toISOString() + file.originalname);
// };

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  dest: "uploads/",
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

router.get("/imgup", (req, res) => {
  res.render("shop/uploads");
});
router.post("/imgup", upload.single("img"), (req, res) => {
  console.log(req.file);
});
module.exports = router;
