const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const path = require("path");
const bodyParser = require("body-parser");
const multer = require("multer");
// Body parser middleware -- descr -- It is used to get the data from the form to process.
// in short we can access the data from thr form through req object.
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.use(express.static(path.join(__dirname, "/public")));

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  dest: "uploads/",
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

// Loading product model
require("../models/products");
const Products = mongoose.model("products");

router.get("/product/add", (req, res, next) => {
  res.render("admin/productadd");
});

// Process the productadd form.
router.post("/productadd", upload.single("image"), (req, res) => {
  console.log(req.file);
  const newProduct = {
    image: req.file.path,
    title: req.body.title,
    description: req.body.description,
    price: req.body.price
  };

  new Products(newProduct).save().then(product => {
    res.redirect("/products");
  });
});

module.exports = router;
