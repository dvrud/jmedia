const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const cartSchema = new Schema({
  title: {
    type: String
  },
  description: {
    type: String
  },
  price: {
    type: String
  },
  productId: {
    type: String,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model("cart", cartSchema);
