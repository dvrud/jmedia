const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const now = new Date();

const productSchema = new Schema({
  image: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  price: {
    type: Number,
    required: true
  },
  time: {
    type: String,
    default: now.getTimezoneOffset()
  }
});

module.exports = mongoose.model("products", productSchema);
