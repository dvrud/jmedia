const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// creating the schema
const imagepathSchema = new Schema({
  image: {
    type: Array,
    required: true
  },
  user: {
    type: String,
    required: true
  }
});

mongoose.model("imagepath", imagepathSchema);
