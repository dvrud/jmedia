const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// creating the schema
const BlogSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  author: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  user: {
    type: String,
    required: true
  }
});

mongoose.model("blogs", BlogSchema);
