module.exports = {
  ensureAuthenticated: (req, res, next) => {
    if (req.isAuthenticated()) {
      return next();
    }
    req.session.oldUrl = req.url;
    req.flash("error_msg", "Not authorized");
    res.redirect("/user/login");
  },
  ensureAdminAuthenticated: (req, res, next) => {
    if (req.isAuthenticated() && req.user.isAdmin) {
      return next();
    }
    req.flash("error_msg", "Not authorized Admin");
    res.redirect("/user/login");
  }
};
