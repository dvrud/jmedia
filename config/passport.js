const LocalStrategy = require("passport-local").Strategy;
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

// Loading user model
require("../models/users");

const Users = mongoose.model("users");

module.exports = passport => {
  passport.use(
    new LocalStrategy({ usernameField: "email" }, (email, password, done) => {
      Users.findOne({
        email: email
      })
        .then(user => {
          if (!user) {
            return done(null, false, { message: "User not found!" });
            //The above func done is taking in error as first parameter, user as second parameter but in this case the user does not exist so we passed false and the third parameter is the message that we want to display.
          }

          bcrypt.compare(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
              return done(null, user);
            } else {
              return done(null, false, { message: "Password does not match" });
            }
          });
        })
        .catch(err => {
          console.log(err);
        });
    })
  );

  // Passport serialize and deserialize user strategy
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    Users.findById(id, function(err, user) {
      done(err, user);
    });
  });
};
