// Bringing in all the installed dependencies through npm command.
const express = require("express");
const exphbs = require("express-handlebars");
const path = require("path");
const mongoose = require("mongoose");
const flash = require("connect-flash");
const session = require("express-session");
const passport = require("passport");
const crypto = require("crypto");
const multer = require("multer");
const GridFsStorage = require("multer-gridfs-storage");
const Grid = require("gridfs-stream");
const methodOverride = require("method-override");
const mongostore = require("connect-mongo")(session);
const app = express();

// Setting up all the middleware below.

// express session middleware
app.use(
  session({
    secret: "supersecret",
    resave: false,
    saveUninitialized: false,
    store: new mongostore({ mongooseConnection: mongoose.connection }),
    cookie: { maxAge: 180 * 10 * 1000 }
  })
);
// passport middleware which is always to be used after the express session middleware
app.use(passport.initialize());
app.use(passport.session());
// flash middleware
app.use(flash());
// Global variables for falsj
app.use((req, res, next) => {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  res.locals.user = req.user || null;
  res.locals.session = req.session;
  // this user object can be used all over to check if someone is logged inor not.
  next();
});
// Handlebar middleware
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

//
// Middleware for bringing in public folder containing css and js files.
app.use("/uploads", express.static("uploads"));
app.use("view/uploads", express.static("uploads"));

app.use(express.static(path.join(__dirname, "/public")));

// Setting the routes through the router functions.
const productsRouter = require("./routes/products");
app.use("/", productsRouter);
// Adding products route
const productaddRouter = require("./routes/productadd");
app.use("/", productaddRouter);
// User routes
const userRouter = require("./routes/user");
app.use("/", userRouter);
// Blogpage route
const blogsRouter = require("./routes/blogs");
app.use("/", blogsRouter);
// Admin routes
const adminRouter = require("./routes/admin");
app.use("/", adminRouter);
// Upload routes
const uploadRouter = require("./routes/upload");
app.use("/", uploadRouter);
// Chat route
const chatRouter = require("./routes/chat");
app.use("/", chatRouter);
// Portfolio route
const portfolioRouter = require("./routes/portfolio");
app.use("/", portfolioRouter);
// Serrvice route
const serviceRouter = require("./routes/services");
app.use("/", serviceRouter);
// gallery routes
const galleryRouter = require("./routes/photogallery");
app.use("/", galleryRouter);
// Homepage routes
const indexRouter = require("./routes/index");
app.use("/", indexRouter);
// Getting in the passport
require("./config/passport")(passport);
// Setting up the mongoose.
// Map global promise
mongoose.Promise = global.Promise;
// Connecting to mongoose database
mongoose
  .connect("mongodb://localhost/local-shoppingcart-db", {
    useNewUrlParser: true
  })
  .then(() => {
    console.log("MongoDB connected");
  })
  .catch(err => console.log(err));

const port = 5000;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

module.exports = app;
